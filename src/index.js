import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import {
    setAccessToken,
    removeAccessToken,
    getAccessToken,
    setSessionEndTime,
    getSessionEndTime,
    removeSessionEndTime,
} from './Helpers/storage';
import { decodeToken } from './Helpers/decodeToken';
import { APP_SESSION_TIME, DELAY } from './constants';
import { AuthenticationApi } from './api/AuthenticationApi';
import App from './Components/App/App';
import PageHeader from './Components/PageHeader/PageHeader';
import LoginPage from './Components/LoginPage/LoginPage';
import TimeboxList from './Components/TimeboxList/TimeboxList';
import ErrorMessage from './Components/ErrorMessage/ErrorMessage';

const root = document.getElementById('root');

class Application extends Component {
    constructor() {
        super();
        this.appSessionTimer = null;
    }
    state = {
        delay: DELAY,
        email: '',
        hasError: false,
        errorMessage: '',
        accessToken: null,
        sessionTime: 0,
    };
    componentDidMount() {
        this.appSessionTimer = setInterval(this.tick, this.state.delay);
        const accessToken = getAccessToken();
        const sessionEndTime = getSessionEndTime();
        if (Date.now() >= sessionEndTime) {
            this.handleLogout();
        }
        if (accessToken) {
            this.setState({ accessToken, email: decodeToken(accessToken).email });
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if (prevState.delay !== this.state.delay) {
            this.startInterval();
        }
    }
    
    componentWillUnmount() {
        clearInterval(this.appSessionTimer);
    }
    
    tick = () => this.setState({ sessionTime: this.state.sessionTime + DELAY });
    
    startInterval = () => {
        clearInterval(this.appSessionTimer);
        this.appSessionTimer = setInterval(this.tick, this.state.delay);
    }

    catchError = error =>
        this.setState({ hasError: true, errorMessage: error.message });
    
    handleLogout = () => {
        removeAccessToken();
        removeSessionEndTime();
        clearInterval(this.appSessionTimer);
        this.setState({ accessToken: null, sessionTime: 0, email: '' });
    }
    
    handleLogin = (credentials) =>
        AuthenticationApi
            .login(credentials)
            .then(({ accessToken }) => {
                this.setState({
                    hasError: false,
                    errorMessage: '',
                    accessToken,
                    email: decodeToken(accessToken).email,
                    sessionTime: 0,
                });
                setAccessToken(accessToken);
                setSessionEndTime(Date.now() + APP_SESSION_TIME);
                this.startInterval();
            })
            .catch(this.catchError);
    
    handleAddUser = (credentials) =>
        AuthenticationApi
            .register(credentials)
            .then(user => console.log(user))
            .catch(this.catchError);
    
    render() {
        const { email, accessToken, hasError, errorMessage } = this.state;
        return (
            <App>
                <PageHeader email={email} onLogout={this.handleLogout} />
                {hasError && <ErrorMessage message={errorMessage} />}
                {(hasError || !accessToken) && <LoginPage onAddUser={this.handleAddUser} onLogin={this.handleLogin} />}
                {accessToken && <TimeboxList accessToken={accessToken}/> }
            </App>
        )
    }
}
ReactDOM.render(<Application />, root);