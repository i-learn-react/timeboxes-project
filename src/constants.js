export const BASE_URL = 'http://localhost:3001';

export const DELAY = 1000;

export const APP_SESSION_TIME = 60 * 60 * DELAY;