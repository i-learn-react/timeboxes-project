const appStorage = window.localStorage;

export const setAccessToken = token => appStorage.setItem('accessToken', token);

export const getAccessToken = () => appStorage.getItem('accessToken');

export const removeAccessToken = () => appStorage.removeItem('accessToken');

export const setSessionEndTime = time => appStorage.setItem('sessionEndTime', time);

export const getSessionEndTime = () => appStorage.getItem('sessionEndTime');

export const removeSessionEndTime = () => appStorage.removeItem('sessionEndTime');