export const withEmpty = (componentClass, customClass) =>
    customClass
        ? `${customClass} ${componentClass}`
        : `${componentClass}`;
