import { BASE_URL } from '../constants';
import {makeApiRequest} from "./makeApiRequest"; 
import { decodeToken } from '../Helpers/decodeToken';

const decodeUserId = accessToken => decodeToken(accessToken).sub; 
export const TimeboxesApi = {};

TimeboxesApi.getAll = async function (accessToken) {
    const userId = decodeUserId(accessToken);
    const response = await makeApiRequest(`${BASE_URL}/timeboxes`, 'GET', null, accessToken);
    return await response.filter(timebox => timebox.userId === userId);
};

TimeboxesApi.add = async function (timebox, accessToken) {
    const userId = decodeUserId(accessToken);
    return await makeApiRequest(`${BASE_URL}/timeboxes`, 'POST', { ...timebox, userId }, accessToken);
};

TimeboxesApi.edit = async function (updatedTimebox, accessToken) {
    const { id } = updatedTimebox;
    const userId = decodeUserId(accessToken);
    return await makeApiRequest(`${BASE_URL}/timeboxes/${id}`, 'PUT', { ...updatedTimebox, userId }, accessToken);
};

TimeboxesApi.delete = async function (id, accessToken) {
    const userId = decodeUserId(accessToken);
    return await makeApiRequest(`${BASE_URL}/timeboxes/${id}`, 'DELETE', { userId }, accessToken);
};

