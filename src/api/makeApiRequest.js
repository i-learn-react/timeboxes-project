export const makeApiRequest = async (url, method = 'GET', body = undefined, accessToken = null) => {
    const options = {
        method,
        headers: {},
    };

    if (method !== 'GET') {
        options.headers = {
            'Content-Type': 'application/json',
        };
        options.body = JSON.stringify(body);
    };
    if (accessToken) {
        options.headers.Authorization = `Bearer ${accessToken}`;
    }
    try {
        const result = await fetch(url, options);
        if (!result.ok) {
            const parsedResponse = await result.json() || {};
            throw (parsedResponse)
                ? new Error(parsedResponse)
                : new Error(`Status ${result.status}. ${result.statusText}`);
        } else {
            return await result.json();
        }
    } catch (error) {
        throw new Error(error);        
    }
}