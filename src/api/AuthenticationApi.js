import { BASE_URL } from "../constants";
import { makeApiRequest } from "./makeApiRequest";

export const AuthenticationApi = {};

AuthenticationApi.login = async function (credentials) {
    return await makeApiRequest(`${BASE_URL}/login`, 'POST', credentials);
}

AuthenticationApi.register = async function (credentials) {
    return await makeApiRequest(`${BASE_URL}/register`, 'POST', credentials);
}
