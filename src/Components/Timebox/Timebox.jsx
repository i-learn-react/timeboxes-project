import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Time, calculateTime, increaseCounter, calculateTimeLeft, calculateProgress } from './TimeboxHelpers';

import Button from '../Button/Button';
import Input from '../Input/Input';
import Clock from '../Clock/Clock';
import ProgressBar from '../ProgressBar/ProgressBar';
import InlineText from '../InlineText/InlineText';
import TitleLevel1 from '../TitleLevel1/TitleLevel1';
import { StyledTimebox, StyledTimeboxButtons, StyledSaveButton, StyledTimeboxButton} from './TimeboxStyles';

const defaultState = {
    isRunning: false,
    isPaused: false,
    breaksCounter: 0,
    timeFromStart: 0,
};

const StyledTimeboxEditor = styled.form`
    border-radius: 5px;
    display: flex;
    flex-direction: column;
`;

const SaveButton = styled(Button)`${StyledSaveButton}`;
const TimeboxButton = styled(Button)`${StyledTimeboxButton}`;

class Timebox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ...defaultState,
        }
    }
    startTimer = (initial) => {
        let lastTick = initial;
        this.counter = setInterval(() => {
            this.setState((prevState) => ({
                timeFromStart: calculateTime(prevState.timeFromStart, lastTick.milliseconds),
            }));
            lastTick = new Time();
        }, 10);
    }
    stopTimer = () => {
        clearInterval(this.counter);
    }
    handleStart = () => {
        this.setState((prevState) => ({
            isRunning: !prevState.isRunning,
        }));
        this.startTimer(new Time());
    }
    handleStop = () => {
        this.setState((prevState) => ({
            ...defaultState,
        }));
        this.stopTimer();
    }
    handlePause = () => {
        this.state.isPaused ? this.startTimer(new Time()) : this.stopTimer();
        this.setState((prevState) => {
            return {
                isPaused: !prevState.isPaused,
                breaksCounter: increaseCounter(!prevState.isPaused, prevState.breaksCounter),
            }
        })
    }
    renderClock = ({ minutes, seconds }) => {
        if (minutes <= 0 && seconds <= 0) {
            this.stopTimer();
            return (
                <div className='clock' >
                    <InlineText className='clock_message'>Gratulacje. Zadanie skończone!</InlineText>
                </div>
            )
        }
        return <Clock minutes={minutes} seconds={seconds} />
    }
    handleRemove = (id) => this.props.onRemove(id)

    handleEdit = () => {
        this.setState({
            id: this.props.id,
            title: this.props.title,
            totalTimeInMinutes: this.props.totalTimeInMinutes,
            isEdited: true,
        })
    }

    handleTitleInput = (event) => {
        this.setState({
            title: event.target.value,
        })
    }
    handleTotalTimeInMinutesInput = (event) => {
        this.setState({
            totalTimeInMinutes: event.target.value,
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.onUpdate({
            id: this.state.id,
            title: this.state.title,
            totalTimeInMinutes: this.state.totalTimeInMinutes,
        });
        this.setState({
            id: null,
            title: '',
            totalTimeInMinutes: '',
            isEdited: false,
        })
    }
    renderEditableTitleAndTime = ({ title, totalTimeInMinutes }) => (
        <StyledTimeboxEditor onSubmit={this.handleSubmit}>
            <Input
                name='task'
                value={title}
                onChange={this.handleTitleInput}
            >
                Zadanie:
            </Input>
            <Input
                name='time'
                type='number'
                value={totalTimeInMinutes}
                onChange={this.handleTotalTimeInMinutesInput}
            >
                Czas w minutach:
            </Input>
            <SaveButton>Zapisz</SaveButton>
        </StyledTimeboxEditor>
    )

    renderTitleAndTime = (title, remainingTime) => (
        <React.Fragment>
            <TitleLevel1>{title}</TitleLevel1>
            {this.renderClock(remainingTime)}
        </React.Fragment>
    )
    renderButtons = (config) => (
        config.map(button => (
            <TimeboxButton
                key={button.label}
                onClick={button.onClick}
                disabled={button.disabled}
            >
                {button.label}
            </TimeboxButton>
        ))
    )
    render() {
        const { className, id, title, totalTimeInMinutes } = this.props;
        const { isRunning, isPaused, isEdited, breaksCounter, timeFromStart } = this.state;
        const taskTime = new Time(totalTimeInMinutes);
        const remainingTime = calculateTimeLeft(taskTime.milliseconds, timeFromStart);
        const progressPercent = calculateProgress(taskTime.milliseconds, timeFromStart).toString();
        const removeEditButtonsConfig = [
            {
                onClick: () => this.handleRemove(id),
                disabled: (isRunning && !isPaused) || isEdited,
                label: 'Usuń',
            }, {
                onClick: this.handleEdit,
                disabled: (isRunning && !isPaused) || isEdited,
                label: 'Edytuj',
            },
        ];
        const startStopPauseButtonsConfig = [
            {
                onClick: this.handleStart,
                disabled: isRunning || isEdited,
                label: 'Start',
            }, {
                onClick: this.handleStop,
                disabled: !isRunning || isEdited,
                label: 'Stop',
            }, {
                onClick: this.handlePause,
                disabled: !isRunning || isEdited,
                label: 'Pause',
            },
        ];
        return (
            <StyledTimebox className={className}>
                <StyledTimeboxButtons >
                    {this.renderButtons(removeEditButtonsConfig)}
                </StyledTimeboxButtons>
                {isEdited
                    ? this.renderEditableTitleAndTime(this.state)
                    : this.renderTitleAndTime(title, remainingTime)
                }
                <ProgressBar percent={progressPercent} isPaused={isPaused} />
                <StyledTimeboxButtons >
                    {this.renderButtons(startStopPauseButtonsConfig)}
                    <InlineText>Liczba przerw: </InlineText>
                    <InlineText>{breaksCounter}</InlineText>
                </StyledTimeboxButtons>
            </StyledTimebox>
        )
    }
}

Timebox.propTypes = {
    className: PropTypes.string,
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    totalTimeInMinutes: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
}

Timebox.defaultProps = {
    className: '',
}

export default Timebox;