export function Time(minutes) {
    if (!minutes) {
        this.milliseconds = Date.now();
    } else {
        this.milliseconds = minutes * 60 * 1000;
    }
}

export function increaseCounter(predicate, value) {
    return predicate ? value + 1 : value;
}

export function toSeconds(milliseconds) {
    return Math.ceil(milliseconds / 1000);
}

export function calculateTimeLeft(totalMilliseconds, timeInMilliseconds) {
    const timeLeft = toSeconds(totalMilliseconds - timeInMilliseconds);
    return {
        minutes: Math.floor(timeLeft / 60),
        seconds: timeLeft % 60,
    };
}

export function calculateProgress(totalMilliseconds, timeInMilliseconds) {
    const result = timeInMilliseconds / totalMilliseconds * 100
    return timeInMilliseconds > totalMilliseconds ? 100 : result.toFixed(2);
}

export function calculateTime(total, last) {
    return total + (new Time().milliseconds - last);
}