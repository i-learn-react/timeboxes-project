import styled, {css} from 'styled-components';

export const StyledTimebox = styled.div`
    border: 1px solid #f15c06;
    border-radius: 5px;
    padding: 1em;
    margin: 1em;
    display: flex;
    flex-direction: column;
`;

export const StyledTimeboxButtons = styled.nav`
    display: flex;
    align-items: center;
`;

export const StyledSaveButton = css`
    align-self: flex-end;
    margin: .5em 0;
`

export const StyledTimeboxButton = css`
    align-self: flex-start;
`
