import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components'

const StyledButton = styled.button`
    cursor: pointer;
    border: 1px solid #f15c06;
    border-radius: 5px;
    background-color: #f15c06;
    color: #fff;
    width: 100px;
    height: 30px;
    font-size: 1rem;
    margin-right: 1em;

    &[disabled] {
        cursor: not-allowed;
        background-color: #ccc;
        border: 1px solid #aaa;
    }
`;

const Button = ({ className, disabled, onClick, children }) => {
    return (
        <StyledButton
            className={className}
            disabled={disabled}
            onClick={onClick}
        >
            {children}
        </StyledButton>
    )
};

Button.propTypes = {
    className: PropTypes.string,
    disabled: PropTypes.bool,
    onClick: PropTypes.func.isRequired,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
}

Button.defaultProps = {
    className: '',
    disabled: false,
    onClick: () => null,
    children: '',
}

export default Button;