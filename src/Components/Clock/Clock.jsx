import React from 'react';
import PropTypes from 'prop-types';
import InlineText from '../InlineText/InlineText';
import styled from 'styled-components';

const padLeft = (finalLength, value) => value.toString().padStart(finalLength, '0');
const padTwo = value => padLeft(2, value);

const StyledClock = styled.div`
    font-size: 2rem;
    color: #f15c06;
    align-self: center;
`;

const Clock = ({ className = '', minutes, seconds }) => (
    <StyledClock className={className}>
        <InlineText className='clock_message'>Pozostało: </InlineText>
        <InlineText className='clock_timer'>{padTwo(minutes)}</InlineText>
        <InlineText className='clock_timer'>:</InlineText>
        <InlineText className='clock_timer'>{padTwo(seconds)}</InlineText>
    </StyledClock>
)

const isZeroOrMore = (props, propName, component) => {
    if (props[propName] < 0) {
        return new Error(`Prop ${propName} in ${component} should be 0 or more!`);
    }
}

Clock.propTypes = {
    className: PropTypes.string,
    minutes: isZeroOrMore,
    seconds: isZeroOrMore,
}

Clock.defaultProps = {
    className: '',
}

export default Clock;