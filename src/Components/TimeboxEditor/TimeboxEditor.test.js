import React from 'react';
import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';

import TimeboxEditor from './TimeboxEditor';

const check = element => () => element;

describe('TimeboxEditor test suite', () => {
    it('should render and allow to add title input', () => {
        render(<TimeboxEditor />);
        const titleLabel = screen.getByLabelText(/co chcesz zrobić/i);
        user.type(titleLabel, 'Foo');
        expect(titleLabel.value).toBe('Foo');
    });

    it('should render title input', () => {
        render(<TimeboxEditor />);
        const timeLabel = screen.getByLabelText(/ile minut na to przeznaczasz/i);
        user.type(timeLabel, '4');
        expect(timeLabel.value).toBe('4');
    });
    
    it('should render Dodaj button', () => {
        render(<TimeboxEditor />);
        const button = screen.getByText(/dodaj/i);
        expect(check(button)).not.toThrow();
    });
});