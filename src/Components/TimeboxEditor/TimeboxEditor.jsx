import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Input from '../Input/Input';
import Button from '../Button/Button';


const defaultStateTimeboxEditor = {
    title: '',
    totalTimeInMinutes: '',
};

const StyledTimeboxEditor = styled.form`
    border: 1px solid #f15c06;
    border-radius: 5px;
    padding: 1em;
    margin: 1em;
    display: flex;
    flex-direction: column;
`;

const AddButton = styled(Button)`
    align-self: flex-end;
    margin-top: .5em;
    margin-right: 0;
`;

class TimeboxEditor extends Component {
    state = { ...defaultStateTimeboxEditor };
    handleTitleInput = (event) => {
        this.setState({
            title: event.target.value,
        })
    }
    handleTotalTimeInMinutesInput = (event) => {
        this.setState({
            totalTimeInMinutes: event.target.value,
        })
    }
    clearForm = () => {
        this.setState({ ...defaultStateTimeboxEditor });
    }
    handleTimeboxCreate = (event) => {
        event.preventDefault();
        this.props.onCreate({
            title: this.state.title,
            totalTimeInMinutes: this.state.totalTimeInMinutes,
        });
        this.clearForm();
    }
    render() {
        const { className } = this.props;
        const { title, totalTimeInMinutes } = this.state;
        return (
            <StyledTimeboxEditor
                className={className}
                onSubmit={this.handleTimeboxCreate}
            >
                <Input
                    name='task'
                    value={title}
                    onChange={this.handleTitleInput}
                >
                    Co chcesz zrobić?
                </Input>
                <Input
                    name='time'
                    type='number'
                    value={totalTimeInMinutes}
                    onChange={this.handleTotalTimeInMinutesInput}
                >
                    Ile minut na to przeznaczasz?
                </Input>
                <AddButton>Dodaj</AddButton>
            </StyledTimeboxEditor>
        );
    }
}

TimeboxEditor.propTypes = {
    className: PropTypes.string,    
}

TimeboxEditor.defaultProps = {
    className: '',    
}

export default TimeboxEditor;