import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import TimeboxEditor from '../TimeboxEditor/TimeboxEditor';
import Timebox from '../Timebox/Timebox';
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary';
import { TimeboxesApi } from '../../api/TimeboxesApi';
import ErrorMessage from '../ErrorMessage/ErrorMessage';

const StyledTimeboxList = styled.div`
    display: flex;
    flex-direction: column;
    overflow-y: scroll;
`;

class TimeboxList extends Component {
    state = {
        timeboxList: [],
        hasError: false,
        errorMessage: '',
    }
    componentDidMount() {
        this.getTimeboxes();
    }
    
    catchError = error =>
        this.setState({ hasError: true, errorMessage: error.message });
    
    getTimeboxes = () =>
        TimeboxesApi
            .getAll(this.props.accessToken)
            .then(timeboxList => this.setState({ timeboxList }))
            .catch(this.catchError);

    handleCreate = (createdTimebox) => 
        TimeboxesApi
            .add(createdTimebox, this.props.accessToken)
            .then(this.getTimeboxes())
            .catch(this.catchError);

    handleRemove = (id) =>
        TimeboxesApi
            .delete(id, this.props.accessToken)
            .then(this.getTimeboxes())
            .catch(this.catchError);

    handleUpdate = (updatedTimebox) =>
        TimeboxesApi
            .edit(updatedTimebox, this.props.accessToken)
            .then(this.getTimeboxes())
            .catch(this.catchError);

    render() {
        const { timeboxList, hasError, errorMessage } = this.state;
        return (
            <StyledTimeboxList>
                <ErrorBoundary message='Something went wrong in Timebox Editor'>
                    <TimeboxEditor
                        onCreate={this.handleCreate}
                    />
                </ErrorBoundary>
                {hasError && <ErrorMessage message={errorMessage} />}
                {timeboxList.map(timebox => (
                    <ErrorBoundary key={timebox.id} message='Something went wrong in Timebox'>
                        <Timebox
                            id={timebox.id}
                            title={timebox.title}
                            totalTimeInMinutes={timebox.totalTimeInMinutes}
                            onRemove={this.handleRemove}
                            onUpdate={this.handleUpdate}
                        />
                    </ErrorBoundary>
                ))}
            </StyledTimeboxList>
        )
    }
}

TimeboxList.propTypes = {
    accessToken: PropTypes.string.isRequired,
}

export default TimeboxList;