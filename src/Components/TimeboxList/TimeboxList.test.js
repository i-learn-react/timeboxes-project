import React from 'react';
import { render, screen } from '@testing-library/react';
import user from '@testing-library/user-event';
import '@testing-library/jest-dom/extend-expect';

import TimeboxList from './TimeboxList';

describe('TimeboxList test suite', () => {
    const addTimebox = () => {
        render(<TimeboxList />);
        const titleInput = screen.getByLabelText(/co chcesz zrobić/i, 'label');
        const timeInput = screen.getByLabelText(/ile minut na to przeznaczasz/i, 'label');
        const addButton = screen.queryByText(/dodaj/i, 'button');
        user.type(titleInput, 'Foo');        
        user.type(timeInput, '1');        
        user.click(addButton);        
    }

    it('Should match snapshot', () => {
        render(<TimeboxList />);
        expect(screen.asFragment).toMatchSnapshot();
    });

    it('should display TimeboxEditor', () => {
        render(<TimeboxList />);
        const titleInput = screen.queryByText(/co chcesz zrobić/i, 'label');
        const timeInput = screen.queryByText(/ile minut na to przeznaczasz/i, 'label');
        const button = screen.queryByText(/dodaj/i, 'button');
        expect(titleInput).toBeInTheDocument();
        expect(timeInput).toBeInTheDocument();
        expect(button).toBeInTheDocument();
    });

    it('should display zero Timeboxes when started', () => {
        render(<TimeboxList />);
        const startButton = screen.queryByText(/start/i, 'button');
        expect(startButton).not.toBeInTheDocument();
    });

    it('should display one Timebox when user adds it', () => {
        addTimebox();
        const startButton = screen.queryByText(/start/i, 'button');
        expect(startButton).toBeInTheDocument();
    });

    it('should allow to edit Timebox when user clicks Edit button', () => {
        addTimebox();
        const editButton = screen.queryByText(/edytuj/i, 'button');
        expect(editButton).toBeInTheDocument();
        user.click(editButton);
        const titleInput = screen.getByLabelText(/zadanie/i, 'label');
        const timeInput = screen.getByLabelText(/czas w minutach/i, 'label');
        const saveButton = screen.queryByText(/zapisz/i, 'button');
        user.type(titleInput, 'Bar');        
        user.type(timeInput, '2');        
        user.click(saveButton);
        expect(screen.getByText(/bar/i)).toBeInTheDocument();
        // screen.debug();
    });
});