import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ErrorMessage from '../ErrorMessage/ErrorMessage';

class ErrorBoundary extends Component {
    state = {
        hasError: false,
    }

    static getDerivedStateFromError(error) {
        return (
            { hasError: true }
        )
    }

    componentDidCatch(error, info) {
        console.log(info);
    }
    
    render() {
        const { children, message } = this.props;

        return this.state.hasError
            ? <ErrorMessage message={message} />
            : children;
    }
}

ErrorBoundary.propTypes = {
    message: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
}


export default ErrorBoundary;