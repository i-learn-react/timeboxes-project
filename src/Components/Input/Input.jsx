import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledFormField = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: baseline;
    margin: .5em 0;
`;

const StyledInput = styled.input`
    flex: 1 1 50%;
    border: 1px solid #f15c06;
    height: 2em;
    padding-left: .5em;
    margin-left: .5em;
    font-size: 1em;
`;

const StyledLabel = styled.label`
    flex: 1 1 50%;
    text-align: right;
`;

const Input = ({ className = '', name = '', type = 'text', step = '0.1', children = '', value, onChange }) => (
    <StyledFormField className={className}>
        <StyledLabel htmlFor={name} >
            <span>{children}</span>
            <StyledInput
                name={name}
                type={type}
                step={step}
                value={value}
                onChange={onChange}
                />
        </StyledLabel>
    </StyledFormField>
);

Input.propTypes = {
    className: PropTypes.string,
    name: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    step: PropTypes.number,
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    onChange: PropTypes.func.isRequired,
}

Input.defaultProps = {
    className: '',
    name: '',
    type: 'text',
    step: 0.1,
    children: '',
    value: '',
    onChange: () => null,
}

export default Input;