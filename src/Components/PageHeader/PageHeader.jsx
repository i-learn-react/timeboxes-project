import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../Button/Button';
import InlineText from '../InlineText/InlineText';

const StyledHeader = styled.div`
    box-sizing: border-box;
    font-family: sans-serif;
    display: flex;
    align-items: baseline;
    justify-content: space-between;
    border-bottom: 1px solid #f15c06;
    margin: 0 1em 1em;
    padding: 1em 0;
`;

const PageHeader = ({email, onLogout}) => {
    return (
        <StyledHeader>
            <InlineText>Witaj, {email}</InlineText>
            {email && <Button onClick={onLogout}>Wyloguj</Button>}
        </StyledHeader>
    );
};

PageHeader.propTypes = {
    email: PropTypes.string,
    onLogout: PropTypes.func.isRequired,
};

PageHeader.defaultProps = {
    email: '',
}

export default PageHeader;