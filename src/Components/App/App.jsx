import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary';

const StyledApp = styled.div`
    font-family: sans-serif;
    display: flex;
    flex-direction: column;
    min-width: 400px;
    max-width: 600px;
    flex: 1 1 50%;
    margin: 0 auto;
    height: 100vh;
`;

const App = ({ className = '', children }) => (
    <ErrorBoundary message='Something went wrong in App component'>
        <StyledApp className={className}>
            {children}
        </StyledApp>
    </ErrorBoundary>
);

App.propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
}

App.defaultProps = {
    className: '',
    children: '',
}

export default App;