import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledProgressBar = styled.div`
    height: 2em;
    border: 1px solid #f15c06;
    margin-top: .5em;
    margin-bottom: 1em;
    padding: 2px;
    filter: ${props => props.isPaused ? 'blur(2px) grayscale()' : ''};
`;

const StyledProgressBarIndicator = styled.div.attrs(props => ({
    style: {
      width: `${props.percent}%`,
    },
  }))`
    background-color: #f15c06;
    height: 100%;
`;

const ProgressBar = ({ className, percent, isPaused }) => {
    return (
        <StyledProgressBar className={className} isPaused={isPaused} >
            <StyledProgressBarIndicator percent={percent} />
        </StyledProgressBar>
    );
}

ProgressBar.propTypes = {
    className: PropTypes.string,
    percent: PropTypes.string.isRequired,
    isPaused: PropTypes.bool,
}

ProgressBar.defaultProps = {
    className: '',
    isPaused: false,
}

export default ProgressBar;