import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledInlineText = styled.span`
`;

const InlineText = ({ className, children }) => (
    <StyledInlineText className={className}>{children}</StyledInlineText>
);

InlineText.propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
}

InlineText.defaultProps = {
    className: '',
}

export default InlineText;