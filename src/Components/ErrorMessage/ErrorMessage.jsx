import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components";

const StyledError = styled.div`
    background-color: #f15c06;
    color: #fff;
    display: flex;
    justify-content: center;
    align-items: center;
    padding-top: 2em;
    padding-bottom: 2em;
`;

const ErrorMessage = ({message}) => {
    return (
        <StyledError >
            {message}
        </StyledError>
    );
};

ErrorMessage.propTypes = {
    message: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
};

export default ErrorMessage;
