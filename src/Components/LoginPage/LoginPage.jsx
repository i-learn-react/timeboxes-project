import React, { Component } from 'react';
import styled from 'styled-components';
import Input from '../Input/Input';
import Button from '../Button/Button';

const StyledLoginPage = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column;
    overflow-y: hidden;
    padding: 1em;
    margin: 1em;
    align-items: flex-end;
`;

const LoginButton = styled(Button)`
    align-self: flex-end;
    margin-top: .5em;
    margin-right: 0;
`;

const AddButton = styled(Button)`
    align-self: flex-end;
    margin-top: .5em;
    margin-right: 1em;
    background-color: transparent;
    color: #f15c06;
    border: none;
`;

const StyledHorizontalContainer = styled.div`
    display: flex;
`;

class LoginPage extends Component {
    state = { email: '', password: '' };

    handleEmailInput = event => this.setState({ email: event.target.value });
    handlePasswordInput = event => this.setState({ password: event.target.value });
    
    render() {
        const { email, password } = this.state;
        return (
            <StyledLoginPage>
                <Input
                    name='email'
                    value={email}
                    onChange={this.handleEmailInput}
                >
                    Email:
                </Input>
                <Input
                    name='password'
                    type='password'
                    value={password}
                    onChange={this.handlePasswordInput}
                >
                    Password:
                </Input>
                <StyledHorizontalContainer>
                    <AddButton
                        onClick={() => {
                            this.props.onAddUser({ email, password });
                            this.setState({ email: '', password: '' });
                        }}
                    >Dodaj</AddButton>
                    <LoginButton onClick={() => this.props.onLogin({ email, password })}>Zaloguj</LoginButton>
                </StyledHorizontalContainer>
            </StyledLoginPage>
        );
    }
};

export default LoginPage;