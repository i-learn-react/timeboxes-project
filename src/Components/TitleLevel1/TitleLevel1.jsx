import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Title1 = styled.h1`
    align-self: center;
`

const TitleLevel1 = ({ className, children }) => <Title1 className={className}>{children}</Title1>

TitleLevel1.propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
}

TitleLevel1.defaultProps = {
    className: '',
}

export default TitleLevel1;